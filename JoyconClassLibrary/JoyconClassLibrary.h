#pragma once
#include "stdafx.h"
#include <JoyconManager.h>
using namespace System;

namespace JoyconClassLibrary {
	public ref class JoyconInterface
	{
		// TODO: Add your methods for this class here.
	public:
		IntPtr GetHandle(bool leftRight);
		void Initialize(IntPtr device);
		void Closing(IntPtr device);
		void Update(IntPtr device);

		uint16_t Get_Buttons(IntPtr device);
		float GetStickXAxis(IntPtr device);
		float GetStickYAxis(IntPtr device);
	};
}
