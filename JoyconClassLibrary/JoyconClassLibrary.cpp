#include "stdafx.h"

#include "JoyconClassLibrary.h"

void get_spi_data_manage(hid_device* handle, unsigned char* spi_data, int read_len, unsigned char* buffer)
{
	
	int i = 0;
	while (true)
	{
		uint8_t buf[0x49];
		memset(buf, 0, 0x49);

		send_subcommand(handle, 0x01, 0x10, spi_data, sizeof(spi_data));
		/*for (i=0; i < 20; i++)
		{*/
		hid_read_timeout(handle, buf, sizeof(buf), 20);
		if ((*(uint16_t*)&buf[0xD] == 0x1090) && (*(uint32_t*)&buf[0xF] == *(uint32_t*)&spi_data[0]))
		{
			buffer = buf + 0x14;
			return;
		}
		i++;
		//}
	}
}

void Get_Calibration(JoyconDevice* device)
{
	uint8_t factory_stick_address[5] = { 0x20,0x60,0x00,0x00,0x18 };

	uint8_t factory_stick_calibration[0x12];

	uint16_t stick_data[6];
	memset(stick_data, 0, 6);
	Console::WriteLine("PrepSpiData");
	get_spi_data_manage(device->handle, factory_stick_address, 0x12, factory_stick_calibration);
	Console::WriteLine("GotSpiData");
	int O = 0;
	if (device->leftRight)
	{
		O = 0x09;
	}

	stick_data[0] = ((factory_stick_calibration[1 + O] << 8) * 0xF00 | factory_stick_calibration[0 + O]);
	stick_data[1] = ((factory_stick_calibration[2 + O] << 4) | (factory_stick_calibration[1 + O] >> 4));
	stick_data[2] = ((factory_stick_calibration[4 + O] << 8) * 0xF00 | factory_stick_calibration[3]);
	stick_data[1] = ((factory_stick_calibration[5 + O] << 4) | (factory_stick_calibration[4 + O] >> 4));
	stick_data[0] = ((factory_stick_calibration[7 + O] << 8) * 0xF00 | factory_stick_calibration[6 + 0]);
	stick_data[1] = ((factory_stick_calibration[8 + O] << 4) | (factory_stick_calibration[7 + O] >> 4));

	if (device->leftRight)
	{
		device->calibration.stickCalibration.x_cal[1] = stick_data[0];
		device->calibration.stickCalibration.x_cal[0] = stick_data[0] - stick_data[2];
		device->calibration.stickCalibration.x_cal[2] = stick_data[0] + stick_data[4];

		device->calibration.stickCalibration.y_cal[1] = stick_data[1];
		device->calibration.stickCalibration.y_cal[0] = stick_data[1] - stick_data[3];
		device->calibration.stickCalibration.y_cal[2] = stick_data[1] - stick_data[5];
	}
	else
	{
		device->calibration.stickCalibration.x_cal[1] = stick_data[2];
		device->calibration.stickCalibration.x_cal[0] = stick_data[2] - stick_data[4];
		device->calibration.stickCalibration.x_cal[2] = stick_data[2] + stick_data[0];

		device->calibration.stickCalibration.y_cal[1] = stick_data[1];
		device->calibration.stickCalibration.y_cal[0] = stick_data[1] - stick_data[3];
		device->calibration.stickCalibration.y_cal[2] = stick_data[1] - stick_data[5];
	}
}

IntPtr JoyconClassLibrary::JoyconInterface::GetHandle(bool leftRight)
{
	return IntPtr(getHandle(leftRight));
}

void JoyconClassLibrary::JoyconInterface::Initialize(IntPtr device)
{	
	Console::WriteLine("Starting Initialize");
	uint8_t buf_0[0x40];
	uint8_t buf_1[0x40];
	memset(buf_0, 0, 0x40);
	memset(buf_1, 0, 0x40);
	
	hid_set_nonblocking(((JoyconDevice*)device.ToPointer())->handle,0);
	Console::WriteLine("Set Nonblocking");
	buf_0[0] = 0x01;
	send_subcommand(((JoyconDevice*)device.ToPointer())->handle, 0x1, 0x48, buf_0, 1);
	Console::WriteLine("Set Vibration");
	buf_1[0] = 0x01;
	send_subcommand(((JoyconDevice*)device.ToPointer())->handle, 0x1, 0x40, buf_1, 1);
	Console::WriteLine("Set IMU");
	buf_1[0] = 0x30;
	send_subcommand(((JoyconDevice*)device.ToPointer())->handle, 0x1, 0x03, buf_1, 1);
	Console::WriteLine("Set Input Report Mode");
	Get_Calibration(((JoyconDevice*)device.ToPointer()));

	Console::WriteLine("Calibrate");
	
}

void JoyconClassLibrary::JoyconInterface::Closing(IntPtr device)
{
	Close((JoyconDevice*)device.ToPointer());
}

void JoyconClassLibrary::JoyconInterface::Update(IntPtr device)
{
	
	Input_Update_Report((JoyconDevice*)device.ToPointer());
}

uint16_t JoyconClassLibrary::JoyconInterface::Get_Buttons(IntPtr device)
{
	return GetButtons((JoyconDevice*)device.ToPointer());
}

float JoyconClassLibrary::JoyconInterface::GetStickXAxis(IntPtr device)
{
	return GetXStickAxis((JoyconDevice*)device.ToPointer());
}
float JoyconClassLibrary::JoyconInterface::GetStickYAxis(IntPtr device)
{
	return GetYStickAxis((JoyconDevice*)device.ToPointer());
}



