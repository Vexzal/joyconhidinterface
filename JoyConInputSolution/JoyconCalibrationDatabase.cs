﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Xml;
using System.Xml.Serialization;

using System.IO;

using System.Text;
using System.Threading.Tasks;

namespace Microsoft.Xna.Framework.Input
{
    public class SerializeContainer
    {
        public JCalState[] calibrations;
    }
    class JoyconCalibrationCollection : IEnumerable<JCalState>
    {
        
        public static JoyconCalibrationCollection Initialize = Open();
        private static JoyconCalibrationCollection Open()
        {
            XmlSerializer jccs = new XmlSerializer(typeof(SerializeContainer));

            SerializeContainer container;

            using (FileStream fileStream = new FileStream(Directory.GetCurrentDirectory() + "JoyconCalibrationDatabase.xml", FileMode.OpenOrCreate))
            {
                try
                {
                    container = (SerializeContainer)jccs.Deserialize(fileStream);
                    return new JoyconCalibrationCollection(container.calibrations);
                }
                catch(Exception)
                {
                    container = new SerializeContainer() { calibrations = new JCalState[0] };
                    return new JoyconCalibrationCollection(container.calibrations);
                }
            }            
        }

        public void Close()
        {
            if (!Modified)
                return;

            XmlSerializer jccs = new XmlSerializer(typeof(SerializeContainer));

            SerializeContainer container = new SerializeContainer() { calibrations = _bindings.ToArray() };

            using (FileStream fileStream = new FileStream(Directory.GetCurrentDirectory() + "JoyconCalibrationDatabase.xml", FileMode.OpenOrCreate))
            {
                jccs.Serialize(fileStream, container);
            }

        }

        private readonly List<JCalState> _bindings;
        private readonly Dictionary<string, int> _indexLookup;

        public bool Modified { get; internal set; }
        //constructors         

        public JoyconCalibrationCollection(JCalState[] calibrations)
        {
            Modified = false;
            _bindings = calibrations.ToList(); //bindings list

            _indexLookup = new Dictionary<string, int>();//max amount of joycons, change later to 8 //Nevermind since you can have more joycons than currently connected, fun!

            for (int i = 0; i < calibrations.Length; i++)
            {
                _indexLookup.Add(_bindings[i].serlialNum, i);
            }            
        }

        public void Add(JCalState state)
        {
            _bindings.Add(state);
            _indexLookup.Add(state.serlialNum, _bindings.Count - 1);
            Modified = true;
        }        

        public bool ContainsKey(string serialNum )
        {
            return _indexLookup.ContainsKey(serialNum);
        }
        public JCalState this[int index]
        {
            get { return _bindings[index]; }
        }
        
        public JCalState this[string serialNumber]
        {
            get
            {
                int index;
                if (_indexLookup.TryGetValue(serialNumber, out index))
                {
                    return _bindings[index];
                }
                return new JCalState();
            }
            
        }

        public IEnumerator<JCalState> GetEnumerator()
        {
            return ((IEnumerable<JCalState>)_bindings).GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return _bindings.GetEnumerator();
        }
    }
}
