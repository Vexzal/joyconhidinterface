﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Microsoft.Xna.Framework.Input
{
    public enum JoyconPairPlayer
    {
        PairOne = 0,
        PairTwo = 1
    }
    public static class JoyconPair
    {        
        private class JoyconPairInfo
        {
            public JoyconIndex LeftIndex;
            public JoyconIndex RightIndex;
        }

        private static Dictionary<int, JoyconPairInfo> JoyconPairs = new Dictionary<int, JoyconPairInfo>();

        public static void AddDevice(JoyconIndex Left, JoyconIndex Right)
        {
            JoyconPairInfo pair = new JoyconPairInfo();

            pair.LeftIndex = Left;
            pair.RightIndex = Right;

            var id = 0;
            while (JoyconPairs.ContainsKey(id))
                id++;
            JoyconPairs.Add(id, pair);

        }

        public static JoyconPairState GetState(JoyconPairPlayer index)
        {

            return GetState((int)index);
        }
        private static JoyconPairState GetState(int index)
        {
            if (!JoyconPairs.ContainsKey(index))
                return JoyconPairState.Default;

            return new JoyconPairState(JoyconPairs[index].LeftIndex, JoyconPairs[index].RightIndex);
        }
    }

    public enum JPairButton
    {
        Down,
        Up,
        Right,
        Left,
        Y,
        X,
        B,
        A,
        LSR,
        LSL,
        RSR,
        RSL,
        L,
        R,
        ZL,
        ZR,
        Minus,
        Plus,
        RStick,
        LStick,
        Home,
        Capture
    }
   
    public class JoyconPairState
    {
        private struct PairMap
        {
            public bool leftRight;
            public JoyconButtons button;
        }
        private Dictionary<JPairButton, PairMap> JMap = new Dictionary<JPairButton, PairMap>()
        {
            {JPairButton.Down,new PairMap(){leftRight = false,button = JoyconButtons.DownY} },
            {JPairButton.Up,new PairMap(){leftRight = false,button = JoyconButtons.UpX} },
            {JPairButton.Right,new PairMap(){leftRight = false,button = JoyconButtons.RightB} },
            {JPairButton.Left,new PairMap(){leftRight = false,button = JoyconButtons.LeftA} },
            {JPairButton.LSL,new PairMap(){leftRight = false,button = JoyconButtons.SL} },
            {JPairButton.LSR,new PairMap(){leftRight = false,button = JoyconButtons.SR} },
            {JPairButton.L,new PairMap(){leftRight = false,button = JoyconButtons.LR} },
            {JPairButton.ZL,new PairMap(){leftRight = false,button = JoyconButtons.ZLR} },
            {JPairButton.Minus,new PairMap(){leftRight = false,button = JoyconButtons.Minus} },
            {JPairButton.Capture,new PairMap(){leftRight = false,button = JoyconButtons.Capture} },
            {JPairButton.LStick,new PairMap(){leftRight = false,button = JoyconButtons.StickL} },

            {JPairButton.Y,new PairMap(){leftRight = true,button = JoyconButtons.DownY} },
            {JPairButton.X,new PairMap(){leftRight = true,button = JoyconButtons.UpX} },
            {JPairButton.B,new PairMap(){leftRight = true,button = JoyconButtons.RightB} },
            {JPairButton.A,new PairMap(){leftRight = true,button = JoyconButtons.LeftA} },
            {JPairButton.RSL,new PairMap(){leftRight = true,button = JoyconButtons.SL} },
            {JPairButton.RSR,new PairMap(){leftRight = true,button = JoyconButtons.SR} },
            {JPairButton.R,new PairMap(){leftRight = true,button = JoyconButtons.LR} },
            {JPairButton.ZR,new PairMap(){leftRight = true,button = JoyconButtons.ZLR} },
            {JPairButton.Plus,new PairMap(){leftRight = true,button = JoyconButtons.Plus} },
            {JPairButton.Home,new PairMap(){leftRight = true,button = JoyconButtons.Home} },
            {JPairButton.RStick,new PairMap(){leftRight = true,button = JoyconButtons.StickR} },

        };
        public static readonly JoyconPairState Default = new JoyconPairState();

        private JoyconState LeftState;
        private JoyconState RightState;

        public bool Connected { get
            {
                return LeftState.IsConnected && RightState.IsConnected;
            }
        }

        private JoyconPairState()
        {
            LeftState = JoyconState.Default;
            RightState = JoyconState.Default;
        }

        internal JoyconPairState(JoyconState leftState, JoyconState rightState)
        {
            LeftState = leftState;
            RightState = rightState;
        }
        internal JoyconPairState(JoyconIndex LeftIndex, JoyconIndex RightIndex)
        {
            LeftState = Joycon.GetState(LeftIndex);
            RightState = Joycon.GetState(RightIndex);
        }

        public bool IsButtonDown(JPairButton button)
        {
            var map = JMap[button];
            if(map.leftRight)
            {
                return RightState.IsButtonDown(map.button);
            }
            else
            {
                return LeftState.IsButtonDown(map.button);
            }
        }
        public bool IsButtonUp(JPairButton button)
        {
            var map = JMap[button];
            if (map.leftRight)
            {
                return RightState.IsButtonUp(map.button);
            }
            else
            {
                return LeftState.IsButtonUp(map.button);
            }
        }
        public Vector2 RightStick()
        {
            return RightState.StickValue();
        }
        public Vector2 LeftStick()
        {
            return LeftState.StickValue();
        }
        public Matrix RightGyroMatrix()
        {
            return RightState.GetGyroMatrix();
        }
        public Matrix LeftGyroMatrix()
        {
            return LeftState.GetGyroMatrix();
        }

        public Vector3 RightGyroVector()
        {
            return RightState.GetGyroVec3();
        }
        public Vector3 LeftGyroVector()
        {
            return LeftState.GetGyroVec3();
        }

        public Vector3 LeftAccel()
        {
            return LeftState.Acclerometer.Component;
        }
        public Vector3 RightAccel()
        {
            return RightState.Acclerometer.Component;
        }


        public static bool operator ==(JoyconPairState left, JoyconPairState right)
        {
            return (left.LeftState == right.LeftState&&
                    left.RightState == right.RightState);
        }
        public static bool operator !=(JoyconPairState left, JoyconPairState right)
        {
            return !(left == right);
        }

    }
}
