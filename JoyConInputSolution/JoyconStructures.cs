﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.InteropServices;
using HidLibrary;
using Microsoft.Xna.Framework;

namespace Microsoft.Xna.Framework.Input
{
    public enum JoyconIndex
    {
        JoyOne = 0,
        JoyTwo = 1,
        JoyThree = 2,
        JoyFour = 3,
    }    

    [Flags]
    public enum JoyconButtons
    {
        DownY = 1<<0,
        UpX = 1<<1,
        RightB = 1<<2,
        LeftA = 1<<3,
        SR = 1<<4,
        SL = 1<<5,
        LR = 1<<6,
        ZLR = 1<<7,
        Minus = 1<<8,
        Plus = 1<<9,
        StickR = 1<<10,
        StickL = 1<<11,
        Home = 1<<12,
        Capture = 1<<13
    }    
    public struct StickCalibration
    {
        public ushort[] stick_cal_x;
        public ushort[] stick_cal_y;
    }
    public struct JoyconStick
    {
        public ushort X;
        public ushort Y;
        public float CalX;
        public float CalY;

        public static bool operator ==(JoyconStick left, JoyconStick right)
        {
            return (left.X == right.X) &&
                (left.Y == right.Y) &&
                (left.CalX == right.CalX) &&
                (left.CalY == right.CalY);
        }
        public static bool operator !=(JoyconStick left, JoyconStick right)
        {
            
            return !(left == right);
        }
    }
    public struct Gyroscope
    {
        
        public float pitch;
        public float yaw;
        public float roll;
        public Offset offset;
        public struct Offset
        {
            public int n;
            public float pitch;
            public float yaw;
            public float roll;

            public static bool operator ==(Offset left, Offset right)
            {
                return (left.n == right.n) &&
                    (left.pitch == right.pitch) &&
                    (left.yaw == right.yaw) &&
                    (left.roll == right.roll);
            }
            public static bool operator !=(Offset left, Offset right)
            {
                return !(left == right);
            }
        }
        
        public static bool operator ==(Gyroscope left, Gyroscope right)
        {
            return (left.pitch == right.pitch) &&
                (left.yaw == right.yaw) &&
                (left.roll == right.roll) &&
                (left.offset == right.offset);
        }
        public static bool operator !=(Gyroscope left, Gyroscope right)
        {
            return !(left == right);
        }

    }
    public struct Acclerometer
    {
        public float x;
        public float y;
        public float z;
        public Vector3 Component { get => new Vector3(x, y, z); }
        public static bool operator ==(Acclerometer left, Acclerometer right)
        {
            return (left.x == right.x) &&
                (left.y == right.y) &&
                (left.z == right.z);
        }
        public static bool operator !=(Acclerometer left, Acclerometer right)
        {
            return !(left == right);
        }
    }
      
    public struct JCalState
    {
        public ushort Deadzone;
        public string serlialNum;
        public StickCalibration stick_cal;
        public Vector3 acc_cal_coeficient;
        public Vector3 gyro_origin;
        public Vector3 gyro_cal_coeficient;
    }

    public struct JoyconState
    {
        public static readonly JoyconState Default = new JoyconState();

        public bool IsConnected { get; internal set; }

        public float PacketNumber { get; internal set; }//figure this out a bit yeah?//just passed the timing byte
        public JoyconButtons Buttons { get; internal set; }

        public bool LeftRight { get; internal set; }

        //no independent dpad
        public JoyconStick ThumbStick { get; internal set; }
        //no independant trigger values
        //need extra information like gyrescope and accelerometer
        public Gyroscope Gyroscope { get; internal set; }
        public Acclerometer Acclerometer { get; internal set; }

        public JoyconState(bool _connected, float _packet,bool _leftRight, JoyconButtons _buttonState, JoyconStick _joyStickState, Gyroscope _gyroscope, Acclerometer _accelerometer)
        {
            IsConnected = _connected;
            PacketNumber = _packet;
            LeftRight = _leftRight;
            Buttons = _buttonState;
            ThumbStick = _joyStickState;
            Gyroscope = _gyroscope;
            Acclerometer = _accelerometer;
        }
        public bool IsButtonDown(JoyconButtons button)
        {
            return (Buttons & button) == button;
        }
        public bool IsButtonUp(JoyconButtons button)
        {
            return (Buttons & button) != button;
        }
        public Vector2 StickValue()
        {
            return new Vector2(ThumbStick.CalX, ThumbStick.CalY);
        }

        public Matrix GetGyroMatrix()
        {
            return Matrix.CreateFromYawPitchRoll(Gyroscope.yaw, Gyroscope.pitch, Gyroscope.roll);
        }
        public Quaternion GetGyroQuad()
        {
            return Quaternion.CreateFromYawPitchRoll(Gyroscope.yaw, Gyroscope.pitch, Gyroscope.roll);
        }
        public Vector3 GetGyroVec3()
        {
            return new Vector3(Gyroscope.pitch, Gyroscope.roll, Gyroscope.yaw);
        }
            public static bool operator ==(JoyconState left, JoyconState right)
        {
            return (left.IsConnected == right.IsConnected) &&
                (left.PacketNumber == right.PacketNumber) &&
                (left.Buttons == right.Buttons) &&
                (left.ThumbStick == right.ThumbStick) &&
                (left.Gyroscope == right.Gyroscope) &&
                (left.Acclerometer == right.Acclerometer);
        }
        public static bool operator !=(JoyconState left, JoyconState right)
        {
            return !(left == right);
        }
    }    
}
