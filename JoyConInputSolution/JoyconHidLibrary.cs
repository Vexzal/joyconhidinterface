﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.IO;
using System.Xml;
using System.Xml.Serialization;
using System.Text;
using System.Threading.Tasks;
using HidLibrary;

namespace Microsoft.Xna.Framework.Input
{

    //when you wake up reorder Joycon index, for more devices
    //add system for creating pairs.
    public static partial class Joycon
    {
        private class JoyconInfo
        {
            public HidFastReadDevice device;
            public bool attatched;
            public bool leftRight;
            Vector3 AccHorizontalOffset;
            //state
            public float packetNum = 0;
            public JoyconButtons joyStates = new JoyconButtons();
            public JoyconStick joyconStick = new JoyconStick();
            public Acclerometer acc_state = new Acclerometer();
            public Gyroscope gyro_state = new Gyroscope();
            //calibration
            public JCalState cal_state = new JCalState()
            {
                
                stick_cal = new StickCalibration() { stick_cal_x = new ushort[3], stick_cal_y = new ushort[3] },
                acc_cal_coeficient = new Vector3(),
                gyro_origin = new Vector3(),
                gyro_cal_coeficient = new Vector3(),
            };
            public JoyconInfo(bool LeftRight)
            {
                leftRight = LeftRight;
                if(leftRight)
                {
                    AccHorizontalOffset = new Vector3(350, 0, 4081);
                }
                else
                {
                    AccHorizontalOffset = new Vector3(350, 0, -4081);
                }
            }
            public void OnReport(HidReport report)
            {
                
                if (attatched == false) { return; }
                var packet = report.Data;
                if (report.ReportId == 0x21 || report.ReportId == 0x30 || report.ReportId == 0x31 || report.ReportId == 0x32 || report.ReportId == 0x33)
                {
                    this.packetNum = packet[0];
                    var button_data = new byte[3];
                    Array.Copy(packet, 2, button_data, 0, 3);
                    var stick_data = new byte[3];
                    if (leftRight)
                    {
                        joyStates = (JoyconButtons)((int)button_data[1] << 8) | (JoyconButtons)(button_data[0]);
                        Array.Copy(packet, 8, stick_data, 0, 3);
                    }
                    if (!leftRight)
                    {
                        joyStates = (JoyconButtons)((int)button_data[1] << 8) | (JoyconButtons)(button_data[2]);
                        Array.Copy(packet, 5, stick_data, 0, 3);
                    }
                    var sensor_data = new byte[12];
                    Array.Copy(packet, 12, sensor_data, 0, 12);
                    
                    joyconStick.X = (ushort)(stick_data[0] | (stick_data[1] & 0xF) << 8);
                    joyconStick.Y = (ushort)((stick_data[1] >> 4) | (stick_data[2] << 4));

                    CalcAnalogStick(out joyconStick.CalX, out joyconStick.CalY, joyconStick.X, joyconStick.Y, cal_state.stick_cal.stick_cal_x, cal_state.stick_cal.stick_cal_y);


                    //sensor 
                    //accelerometer
                    acc_state.x = (float)(short)(sensor_data[0] | (sensor_data[1] << 8) & 0xFF00) * cal_state.acc_cal_coeficient.X;
                    acc_state.y = (float)(short)(sensor_data[2] | (sensor_data[3] << 8) & 0xFF00) * cal_state.acc_cal_coeficient.Y;
                    acc_state.z = (float)(short)(sensor_data[4] | (sensor_data[5] << 8) & 0xFF00) * cal_state.acc_cal_coeficient.Z;

                    //acc_state.x = (float)((short)(sensor_data[0] | (sensor_data[1] << 8) & 0xFF00) -  AccHorizontalOffset.X)* /*0.000244f*/ cal_state.acc_cal_coeficient.X;
                    //acc_state.y = (float)((short)(sensor_data[2] | (sensor_data[3] << 8) & 0xFF00) - AccHorizontalOffset.Y )* /*0.000244f*/ cal_state.acc_cal_coeficient.Y;
                    //acc_state.z = (float)((short)(sensor_data[4] | (sensor_data[5] << 8) & 0xFF00) -AccHorizontalOffset.Z)* /*0.000244f*/ cal_state.acc_cal_coeficient.Z;

                    //alright thats getting core data lets now do the, deadzone conversion
                    gyro_state.roll = (float)((short)(sensor_data[6] | (sensor_data[7] << 8) & 0xFF00) - cal_state.gyro_origin.X) * cal_state.gyro_cal_coeficient.X;

                    

                    gyro_state.pitch = (float)((short)(sensor_data[8] | (sensor_data[9] << 8) & 0xFF00) - cal_state.gyro_origin.Y )* cal_state.gyro_cal_coeficient.Y;

                    gyro_state.yaw = (float)((short)(sensor_data[10] | (sensor_data[11] << 8) & 0xFF00) - cal_state.gyro_origin.Z) * cal_state.gyro_cal_coeficient.Z;

                    if (Math.Abs(gyro_state.roll) < .04)
                        gyro_state.roll = 0;
                    if (Math.Abs(gyro_state.pitch )< .04)
                        gyro_state.pitch= 0;
                    if (Math.Abs(gyro_state.yaw)< .04)
                        gyro_state.yaw = 0;

                    //gyro_state.roll = (float)(short)(sensor_data[6] | (sensor_data[7] << 8) & 0xFF00) * 0.070f;

                    //gyro_state.pitch = (float)(short)(sensor_data[8] | (sensor_data[9] << 8) & 0xFF00) * 0.070f;

                    //gyro_state.yaw = (float)(short)(sensor_data[10] | (sensor_data[11] << 8) & 0xFF00) * 0.070f;

                }
                device.FastReadReport(OnReport);
            }

            void CalcAnalogStick(out float OutX, out float OutY, ushort x, ushort y, ushort[] x_calc, ushort[] y_calc, ushort Deadzone)
            {
                OutX = 0;
                OutY = 0;
                //okay so lets get centered values
                float xDiff = MathHelper.Clamp(x, x_calc[0], x_calc[2]);

                 xDiff = x - x_calc[1];//this should center our value which gives us a deadzone mode.
                float yDiff = MathHelper.Clamp(y, y_calc[0], y_calc[2]);
                yDiff = y - y_calc[1];

                float xMax = x_calc[1] + x_calc[2];
                float xMin = x_calc[1] - x_calc[0];
                var xRange = xMin + xMax;
                float yMax = y_calc[1] + y_calc[2];
                float yMin = y_calc[1] - y_calc[0];
                var yRange = yMin + yMax;
                //so. the output should be like, what
                //okay so our deadzone value is our mixed value
                float magnitude = (float)Math.Sqrt(xDiff * xDiff + yDiff * yDiff);

                //okay so. we have our magnitude lets check it against our deadzone
                if(magnitude > Deadzone)
                {
                    if(xDiff > 0)
                    {
                        OutX = xDiff / xMax;
                    }
                    else
                    {
                        OutX = xDiff / xMin;
                    }
                    if(yDiff > 0)
                    {
                        OutY = yDiff / yMax;
                    }
                    else
                    {
                        OutY = yDiff / yMin;
                    }
                }
                else
                {
                   
                }
            }

            void CalcAnalogStick(out float OutX, out float OutY, ushort x, ushort y, ushort[] x_calc, ushort[] y_calc)
            {
                OutX = 0;
                OutY = 0;
                
                float x_f, y_f;
                float deadZoneCenter = 0.15f;
                float deadZOneOuter = .10f;
                x = (ushort)MathHelper.Clamp(x, x_calc[0], x_calc[2]);
                y = (ushort)MathHelper.Clamp(y, y_calc[0], y_calc[2]);
                if (x >= x_calc[1])
                {
                    x_f = (float)(x - x_calc[1]) / (float)(x_calc[2] - x_calc[1]);
                }
                else
                {
                    x_f = -(float)(x - x_calc[1]) / (float)(x_calc[0] - x_calc[1]);
                }
                if (y >= y_calc[1])
                {
                    y_f = (float)(y - y_calc[1]) / (float)(y_calc[2] - y_calc[1]);
                }
                else
                {
                    y_f = -((float)(y - y_calc[1]) / (float)(y_calc[0] - y_calc[1]));
                }

                float magnitude = (float)Math.Sqrt(x_f * x_f + y_f * y_f);//hey pythagorus theorum
                if (magnitude > deadZoneCenter)
                {
                    float range = 1f - deadZOneOuter - deadZoneCenter;
                    float normalizdMag = Math.Min(1.0f, (magnitude - deadZoneCenter) / range);
                    float scale = normalizdMag / magnitude;
                    OutX = (float)Math.Round( x_f * scale,2);
                    OutY = (float)Math.Round(y_f * scale,2);
                }
                else
                {
                    OutX = 0;
                    OutY = 0;
                }
                

                

            }

            //void CalcAnalogStick(out float OutX, out float OutY, ushort x, ushort y, ushort[] x_calc,ushort[] y_calc,ushort deadzone)
            //{
            //    OutX = 0;
            //    OutY = 0;
            //    //okay so we move to the center, right?
            //    float diffX = x - x_calc[1];
            //    float diffY = y - y_calc[1];

            //    if(Math.Abs(diffX) > deadzone)
            //    {
            //        if (diffX > 0)
            //            OutX = diffX / x_calc[0];
            //        else
            //            OutX = diffX / x_calc[2];
            //    }
            //    if (Math.Abs(diffY) > deadzone)
            //    {
            //        if (diffY > 0)
            //            OutY = diffY / y_calc[0];
            //        else
            //            OutY = diffY / y_calc[2];
            //    }

            //}

            public void DeviceAttachedHandler()
            {
                attatched = true;
                Console.WriteLine("Gamepad Attached.");
                //device.ReadReport(OnReport);
                // _rightDevice.ReadReport(OnReport);
            }
            public void DeviceRemovedHandler()
            {
                attatched = false;
                Console.WriteLine("Gamepad removed.");
            }
        }

        private static JoyconCalibrationCollection calibrationDB;

        private static Dictionary<int, JoyconInfo> Joycons = new Dictionary<int, JoyconInfo>();

        private const int MaxJoycons = 8;

        public static void Initialize()
        {
            //Open database here
            calibrationDB = JoyconCalibrationCollection.Initialize;


            //also get device list here and pass through

            //AddDevice(true);
            //AddDevice(false);
            HidFastReadEnumerator enumerator = new HidFastReadEnumerator();

            var JoyconDeviceList = enumerator.Enumerate(0x057e, 0x2006, 0x2007);

            int indexer = Math.Min(MaxJoycons, JoyconDeviceList.Count());

            int i = 0;
            foreach(IHidDevice device in JoyconDeviceList)
            {
                if (i > indexer)
                    break;

                AddDevice((HidFastReadDevice)device);

                i++;
            }


        }

        internal static void AddDevice(HidFastReadDevice device)
        {
            //get serial num
            string serial = "";
            byte[] sArray;
            device.ReadSerialNumber(out sArray);
            for(int i=0;i<=22;i+=2)
            {
                serial += BitConverter.ToChar(sArray, i);
            }

            bool leftRight = (device.Attributes.ProductId == 0x2007);

            var Joycon = new JoyconInfo(leftRight);

            Joycon.leftRight = leftRight;
            Joycon.device = device;
            Joycon.attatched = true;

            var id = 0;
            while (Joycons.ContainsKey(id))
                id++;
            Joycons.Add(id, Joycon);

            Joycon.device.Inserted += Joycon.DeviceAttachedHandler;
            Joycon.device.Removed += Joycon.DeviceRemovedHandler;
            Joycon.device.MonitorDeviceEvents = true;
            byte[] buf = new byte[1] { 0x01 };
            //enable vibration
            send_subcommand(0x01, 0x48, buf, 1, Joycon.device);
            //enable IMU
            send_subcommand(0x01, 0x40, buf, 1, Joycon.device);
            //set report mode
            buf[0] = 0x30;
            send_subcommand(0x01, 0x03, buf, 1, Joycon.device);

            if(calibrationDB.ContainsKey(serial))
            {
                Joycon.cal_state = calibrationDB[serial];
            }
            else
            {
                Joycon.cal_state.serlialNum = serial;
                GetCalibration(Joycon);
                calibrationDB.Add(Joycon.cal_state);
            }
            Joycon.device.FastReadReport(Joycon.OnReport);

        }
        [Obsolete]
        internal static void AddDevice(bool leftRight)
        {
            var Joycon = new JoyconInfo(leftRight);
            #region establish device
            HidFastReadEnumerator enumerator = new HidFastReadEnumerator();

            if(leftRight)
            {
                Joycon.device= (HidFastReadDevice)enumerator.Enumerate(0x057e, 0x2007).FirstOrDefault();
                Joycon.leftRight = leftRight;                
            }
            else
            {
                Joycon.device = (HidFastReadDevice)enumerator.Enumerate(0x057e, 0x2006).FirstOrDefault();
                Joycon.leftRight = leftRight;
            }
            #endregion
            if (Joycon.device != null)
            {
                Joycon.attatched = true;

                var id = 0;
                while (Joycons.ContainsKey(id))
                    id++;
                Joycons.Add(id, Joycon);
                #region deviceinit
                //device attached handlers
                Joycon.device.Inserted += Joycon.DeviceAttachedHandler;
                Joycon.device.Removed += Joycon.DeviceRemovedHandler;
                //Monitor events
                Joycon.device.MonitorDeviceEvents = true;

                byte[] buf = new byte[1] { 0x01 };
                //enable vibration
                send_subcommand(0x01, 0x48, buf, 1, Joycon.device);
                //enable IMU
                send_subcommand(0x01, 0x40, buf, 1, Joycon.device);

                buf[0] = 0x30;
                //set report mode
                send_subcommand(0x01, 0x03, buf, 1, Joycon.device);
                #endregion
                GetCalibration(Joycon);
                
                Joycon.device.FastReadReport(Joycon.OnReport);
            }

        }

        private static void DisposeDevice(JoyconInfo info)
        {
            info.attatched = false;
            info.device.CloseDevice();
        }
        public static void CloseDevices()
        {
            foreach(var entry in Joycons)
            {
                DisposeDevice(entry.Value);
            }
            calibrationDB.Close();
            Joycons.Clear();
        }

        public static JoyconState GetState(JoyconIndex Joycon)
        {
            return GetState((int)Joycon);
        }
        public static JoyconState GetState(int index)
        {
            if (!Joycons.ContainsKey(index))
                return JoyconState.Default;

            return new JoyconState(
                Joycons[index].attatched,
                Joycons[index].packetNum,
                Joycons[index].leftRight,
                Joycons[index].joyStates,
                Joycons[index].joyconStick,
                Joycons[index].gyro_state,
                Joycons[index].acc_state);
        }

    }
    public static partial class Joycon
    {
        private static void send_command(int command, byte[] data, int len, HidFastReadDevice joycon)
        {
            byte[] buffer = new byte[1 + data.Length];
            for (int i = 0; i < buffer.Length; i++)
            {
                buffer[i] = 0;
            }
            buffer[0x0] = (byte)command;
            if (len != 0)
            {
                for (int i = 0; i < data.Length; i++)
                {
                    buffer[i + 1] = data[i];
                }
            }
            joycon.Write(buffer);

        }
        static byte global_count = 0;
        private static void send_subcommand(int command, int subcommand, byte[] data, int len, HidFastReadDevice joycon)
        {
            byte[] buffer = new byte[0x40];
            for (int i = 0; i < buffer.Length; i++)
            {
                buffer[i] = 0;
            }
            //rumble
            byte[] rumble_base = new byte[9] { (byte)((int)(global_count++) & (int)0xF), 0x00, 0x01, 0x40, 0x40, 0x00, 0x01, 0x40, 0x40 };
            if (global_count > 0xF)
                global_count = 0x0;

            //copy rumble into buffer
            for (int i = 0; i < rumble_base.Length; i++)
            {
                buffer[i] = rumble_base[i];
            }

            buffer[9] = (byte)subcommand;

            //copy data into buffer?
            for (int i = 0; i < data.Length; i++)
            {
                buffer[i + 10] = data[i];
            }
            send_command(command, buffer, 10 + len, joycon);
        }
        private static void GetSpiData(HidFastReadDevice device, byte[] spi_address, byte length, ref byte[] spi_buffer)
        {
            
            HidReport spiReport;
            byte[] spiPacket;
            while (true)
            {
                send_subcommand(0x01, 0x10, spi_address, 1, device);

                //spiReport = device.ReadReport(20);
                for (int i = 0; i < 20; i++)
                {
                    spiReport = device.FastReadReport(20);
                    spiPacket = spiReport.Data;

                    if ((spiPacket[0xD] << 8 | spiPacket[0xC]) == 0x1090 && (spiPacket[0xE] << 8 | spiPacket[0xF]) == (spi_address[0] << 8 | spi_address[1])/* && spiPacket[0xF] == spiAddress[1]*/)
                        goto PassData;
                }
            }
            PassData:
            Array.Copy(spiPacket, 0x13, spi_buffer, 0, length);
        }

        private static void GetCalibration(JoyconInfo info)
        {
            byte[] factory_stick_address = (info.leftRight) ? new byte[5] { 0x46, 0x60, 0x00, 0x00, 0x09 } : new byte[5] { 0x3D, 0x60, 0x00, 0x00, 0x09 };
            byte[] factory_sensor_address = new byte[5] { 0x20, 0x60, 0x00, 0x00, 0x18 };
            byte[] factory_deadzone_address = new byte[5]
                {0x86,0x60,0x00,0x00,0x06 };

            byte[] factory_stick_calibration = new byte[0x09];
            byte[] factory_sensor_calibration = new byte[0x18];
            byte[] factory_deadzone = new byte[0x06];
            ushort[] stick_data = new ushort[6];
            short[,] sensor_cal = new short[2, 3];

            GetSpiData(info.device, factory_stick_address, 0x09, ref factory_stick_calibration);
            GetSpiData(info.device, factory_sensor_address, 0x18, ref factory_sensor_calibration);
            GetSpiData(info.device, factory_deadzone_address, 0x06, ref factory_deadzone);

            //stick
            stick_data[0] = (ushort)((factory_stick_calibration[1] << 8) & 0xF00 | factory_stick_calibration[0]);
            stick_data[1] = (ushort)((factory_stick_calibration[2] << 4) | (factory_stick_calibration[1] >> 4));
            stick_data[2] = (ushort)((factory_stick_calibration[4] << 8) & 0xF00 | factory_stick_calibration[3]);
            stick_data[3] = (ushort)((factory_stick_calibration[5] << 4) | (factory_stick_calibration[4] >> 4));
            stick_data[4] = (ushort)((factory_stick_calibration[7] << 8) & 0xF00 | factory_stick_calibration[6]);
            stick_data[5] = (ushort)((factory_stick_calibration[8] << 4) | (factory_stick_calibration[7] >> 4));

            if (info.leftRight)
            {
                info.cal_state.stick_cal.stick_cal_x[1] = stick_data[0];
                info.cal_state.stick_cal.stick_cal_x[0] = (ushort)(stick_data[0] - stick_data[2]);
                info.cal_state.stick_cal.stick_cal_x[2] = (ushort)(stick_data[0] + stick_data[4]);
                info.cal_state.stick_cal.stick_cal_y[1] = stick_data[1];
                info.cal_state.stick_cal.stick_cal_y[0] = (ushort)(stick_data[1] - stick_data[3]);
                info.cal_state.stick_cal.stick_cal_y[2] = (ushort)(stick_data[1] + stick_data[5]);                
            }
            else//left
            {
                info.cal_state.stick_cal.stick_cal_x[1] = stick_data[2];
                info.cal_state.stick_cal.stick_cal_x[0] = (ushort)(stick_data[2] - stick_data[4]);
                info.cal_state.stick_cal.stick_cal_x[2] = (ushort)(stick_data[2] + stick_data[0]);
                info.cal_state.stick_cal.stick_cal_y[1] = stick_data[3];
                info.cal_state.stick_cal.stick_cal_y[0] = (ushort)(stick_data[3] - stick_data[5]);
                info.cal_state.stick_cal.stick_cal_y[2] = (ushort)(stick_data[3] + stick_data[1]);                
            }

            info.cal_state.Deadzone = (ushort)((factory_deadzone[5]<<8)& 0xF00 | factory_deadzone[4]);

            //accelerometer
            //accelerometer origin
            sensor_cal[0, 0] = (short)(factory_sensor_calibration[0] | factory_sensor_calibration[1] << 8);
            sensor_cal[0, 1] = (short)(factory_sensor_calibration[2] | factory_sensor_calibration[3] << 8);
            sensor_cal[0, 2] = (short)(factory_sensor_calibration[4] | factory_sensor_calibration[5] << 8);
            //short sensiorOriginX = (short)(factory_sensor_calibration[6] | factory_sensor_calibration[7] << 8);
            //short sensiorOriginY = (short)(factory_sensor_calibration[8] | factory_sensor_calibration[9] << 8);
            //short sensiorOriginZ = (short)(factory_sensor_calibration[10] | factory_sensor_calibration[11] << 8);
            //gyrescope 
            sensor_cal[1, 0] = (short)(factory_sensor_calibration[12] | factory_sensor_calibration[13] << 8);
            sensor_cal[1, 1] = (short)(factory_sensor_calibration[14] | factory_sensor_calibration[15] << 8);
            sensor_cal[1, 2] = (short)(factory_sensor_calibration[16] | factory_sensor_calibration[17] << 8);
            //accelerometer in G's (?)

            info.cal_state.acc_cal_coeficient.X = (float)(1.0 / (float)(16384 - sensor_cal[0, 0])) * 4.0f /** 9.8f*/;
            info.cal_state.acc_cal_coeficient.Y = (float)(1.0 / (float)(16384 - sensor_cal[0, 1])) * 4.0f/* * 9.8f*/;
            info.cal_state.acc_cal_coeficient.Z = (float)(1.0 / (float)(16384 - sensor_cal[0, 2])) * 4.0f /** 9.8f*/;
            //Vector3 acc_raw = new Vector3(sensor_cal[0, 0], sensor_cal[0, 1], sensor_cal[0, 2]);
            //info.cal_state.acc_cal_coeficient = acc_raw * 0.000244f;

            //gyroscope degrees per second converted to radians
            info.cal_state.gyro_origin.X = sensor_cal[1, 0];
            info.cal_state.gyro_origin.Y = sensor_cal[1, 1];
            info.cal_state.gyro_origin.Z = sensor_cal[1, 2];
            info.cal_state.gyro_cal_coeficient.X = MathHelper.ToRadians((float)(936 / (float)(13371 - sensor_cal[1, 0])));
            info.cal_state.gyro_cal_coeficient.Y = MathHelper.ToRadians((float)(936 / (float)(13371 - sensor_cal[1, 1])));
            info.cal_state.gyro_cal_coeficient.Z = MathHelper.ToRadians((float)(936 / (float)(13371 - sensor_cal[1, 2])));

        }
    }
}
