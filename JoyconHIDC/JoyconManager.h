#pragma once
#include <stdint.h>
#include "hidapi.h"
#define EXPORT __declspec(dllexport)
//get spi
//calibration
//on report
//starting
//sotring data
//data structs

template <typename T> T CLAMP(const T& value, const T& low, const T& high)
{
	return value < low ? low : (value > high ? high : value);
}
//okay lets just worry about buttons and stick for now I need to solve six axis 
struct JoyconStick {

	uint16_t raw_x = 0;
	uint16_t raw_y = 0;

	float x = 0;
	float y = 0;
};

struct JoyconState
{
	uint16_t buttons;
	JoyconStick stick;
};

struct JoyconCalibration
{
	struct StickCalibration
	{
		uint16_t x_cal[3];
		uint16_t y_cal[3];
	}stickCalibration;
};
struct JoyconDevice {
	//add extern functions too I guess or just//noo that stays out here
	hid_device* handle;
	JoyconState state;
	JoyconCalibration calibration;
	bool running = true;
	bool leftRight = false;
	void UpdateController()
	{

		unsigned char buf[0x170/*?*/];
		hid_read_timeout(handle, buf, 0x170, 20);

	}
};

//class JoyconManager
//{
//public:
	EXPORT void init_bt(JoyconDevice*);
	EXPORT JoyconDevice* getHandle(bool);
	EXPORT void Input_Update_Report(JoyconDevice*);
	EXPORT void SetLight(JoyconDevice*, int);
	EXPORT void Rumble(JoyconDevice*, int);
	EXPORT void Close(JoyconDevice*);

	EXPORT uint16_t GetButtons(JoyconDevice*);
	EXPORT float GetXStickAxis(JoyconDevice*);
	EXPORT float GetYStickAxis(JoyconDevice*);

//private:
	void get_spi_data(hid_device* handle, unsigned char* spi_data, int length, unsigned char*spi_buf);
	void GetCalibration(JoyconDevice* device);
	void CalcAnalogStick(float &pOutX, float &pOutY, uint16_t x, uint16_t y, uint16_t* x_cal, uint16_t* y_cal);
	void send_command(hid_device* handle, int command, uint8_t* data, int len);
	void send_subcommand(hid_device* handle, int command, int subcommand, uint8_t* data, int len);

//};



