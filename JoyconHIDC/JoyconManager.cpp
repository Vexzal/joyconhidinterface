

#include<math.h>
#include "hidapi.h"
#include "JoyconManager.h"

#include <iostream>

using namespace std;

#define JOYCON_VENDER_ID 0x057e
#define JOYCON_LEFT_BT 0x2006
#define JOYCON_RIGHT_BT 0x2007
//left right 1= right 2 = left
#define PI 3.14159265359

int global_count = 0;
//joyconstate left
//joycon state right
//something, different maybe
//if I just, store all handle information in the pointer object I'm holding
//then can't I just.
//also put the update function in there for now?
//halkjsd;f


unsigned char buf[0x40];
JoyconState joystates[];
//remove, hate it, get it gone
void hid_exchange(hid_device* handle, unsigned char* buf, int length)
{
	if (!handle) return;
	hid_write(handle, buf, length);
	hid_read(handle, buf, 0x40);
}

void send_command(hid_device* handle, int command, uint8_t* data, int length)
{
	unsigned char buf[0x40];
	memset(buf, 0, 0x40);

	buf[0x0] = command;
	if (data != nullptr && length != 0)
	{
		memcpy(buf + (0x1), data, length);
	}

	hid_write(handle, buf, length + 0x1);

	/*hid_exchange(handle, buf, length + (0x1));
	
	if (data) {
		memcpy(data, buf, 0x40);
	}*/
}
void send_subcommand(hid_device* handle, int command, int subcommand, uint8_t* data, int length)
{
	unsigned char buf[0x40];
	memset(buf, 0, 0x40);
	uint8_t rumble_base[9] = { (global_count++) & 0xf, 0x00,0x01,0x40,0x40,0x00,0x01,0x40,0x40 };
	memcpy(buf, rumble_base, 9);

	if (global_count >= 0xf) {
		global_count = 0x0;
	}
	buf[9] = subcommand;
	if (data && length != 0)
	{
		memcpy(buf + 10, data, length);
	}
	send_command(handle, command, buf, 10 + length);
}

void get_spi_data(hid_device* handle, unsigned char* spi_data, int read_len, unsigned char* buffer)
{
	uint8_t buf[0x49];
	memset(buf, 0, 0x49);
	int i = 0;
	while (true)
	{
		send_subcommand(handle, 0x01, 0x10, spi_data, sizeof(spi_data));
		/*for (i=0; i < 20; i++)
		{*/
			hid_read_timeout(handle, buf, sizeof(buf), 20);
			if ((*(uint16_t*)&buf[0xD] == 0x1090) && (*(uint32_t*)&buf[0xF] == *(uint32_t*)&spi_data[0]))
			{
				buffer = buf + 0x14;
				return;
			}
			
		//}
	}
}

void GetCalibration(JoyconDevice* device)
{
	uint8_t factory_stick_address[5] = { 0x20,0x60,0x00,0x00,0x18 };
	
	uint8_t factory_stick_calibration[0x12];

	uint16_t stick_data[6];
	memset(stick_data, 0, 6);

	get_spi_data(device->handle, factory_stick_address, 0x12, factory_stick_calibration);

	int O = 0;
	if (device->leftRight)
	{
		O = 0x09;
	}

	stick_data[0] = ((factory_stick_calibration[1+O] << 8) * 0xF00 | factory_stick_calibration[0+O]);
	stick_data[1] = ((factory_stick_calibration[2+O] << 4) | (factory_stick_calibration[1+O] >> 4));
	stick_data[2] = ((factory_stick_calibration[4+O] << 8) * 0xF00 | factory_stick_calibration[3]);
	stick_data[1] = ((factory_stick_calibration[5+O] << 4) | (factory_stick_calibration[4+O] >> 4));
	stick_data[0] = ((factory_stick_calibration[7+O] << 8) * 0xF00 | factory_stick_calibration[6+0]);
	stick_data[1] = ((factory_stick_calibration[8+O] << 4) | (factory_stick_calibration[7+O] >> 4));

	if (device->leftRight)
	{
		device->calibration.stickCalibration.x_cal[1] = stick_data[0];
		device->calibration.stickCalibration.x_cal[0] = stick_data[0] - stick_data[2];
		device->calibration.stickCalibration.x_cal[2] = stick_data[0] + stick_data[4];

		device->calibration.stickCalibration.y_cal[1] = stick_data[1];
		device->calibration.stickCalibration.y_cal[0] = stick_data[1] - stick_data[3];
		device->calibration.stickCalibration.y_cal[2] = stick_data[1] - stick_data[5];
	}
	else
	{
		device->calibration.stickCalibration.x_cal[1] = stick_data[2];
		device->calibration.stickCalibration.x_cal[0] = stick_data[2] - stick_data[4];
		device->calibration.stickCalibration.x_cal[2] = stick_data[2] + stick_data[0];

		device->calibration.stickCalibration.y_cal[1] = stick_data[1];
		device->calibration.stickCalibration.y_cal[0] = stick_data[1] - stick_data[3];
		device->calibration.stickCalibration.y_cal[2] = stick_data[1] - stick_data[5];
	}

}

void CalcAnalogStick(float& pOutX, float& pOutY, uint16_t x, uint16_t y, uint16_t* x_calc, uint16_t* y_calc)
{
	pOutX = 0;
	pOutY = 0;

	float xFactor, yFactor;
	float deadZoneCenter = 0.15f;
	float deadZoneOuter = .1f;

	x = CLAMP(x, x_calc[0], y_calc[2]);
	y = CLAMP(y, y_calc[0], y_calc[2]);

	if (x >= x_calc[1])
	{
		xFactor = (float)(x - x_calc[1]) / (float)(x_calc[2] - x_calc[1]);
	}
	else
	{
		xFactor = -(float)(x - x_calc[1]) / (float)(x_calc[0] - x_calc[1]);
	}
	if (y >= y_calc[1])
	{
		yFactor = (float)(y - y_calc[1]) / (float)(y_calc[2] - y_calc[1]);
	}
	else
	{
		yFactor = -(float)(y - y_calc[1]) / (float)(y_calc[0] - y_calc[1]);
	}

	float magnitude = (float)sqrtf(xFactor * xFactor + yFactor * yFactor);
	if (magnitude > deadZoneCenter)
	{
		float range = 1.0f - deadZoneOuter;
		float normalizedMagnitude = fmin(1.0f, (magnitude - deadZoneCenter) / range);
		float scale = normalizedMagnitude / magnitude;
		pOutX = xFactor * scale;
		pOutY = yFactor * scale;
	}
	else
	{
		pOutX = 0;
		pOutY = 0;
	}
}


//functions accesible through c# side

	EXPORT void SetLight(JoyconDevice* device, int light)
	{

	}
	EXPORT void Rumble(JoyconDevice* device, int frequency)
	{

	}
	EXPORT JoyconDevice* getHandle(bool left_right)
	{
		struct hid_device_info *devices, *cur_device;
		
		JoyconDevice *device, d;
		device = &d;
		if (left_right)
		{
			devices = hid_enumerate(JOYCON_VENDER_ID, JOYCON_RIGHT_BT);
			if (devices->vendor_id == JOYCON_VENDER_ID)
			{
				if (devices->product_id == JOYCON_RIGHT_BT)
				{
					cout << "FoundDevice";
					device->handle = hid_open_path(devices->path);	
					device->leftRight = left_right;
					//init_bt(device);

					return device;
					
				}
			}
		}
		else
		{
			devices = hid_enumerate(JOYCON_VENDER_ID, JOYCON_LEFT_BT);
			if (devices->vendor_id == JOYCON_VENDER_ID)
			{
				if (devices->product_id == JOYCON_LEFT_BT)
				{
					device->handle = hid_open_path(devices->path);
					device->leftRight = left_right;					
					return device;
				}
			}
		}		
	}
	EXPORT void init_bt(JoyconDevice* device)
	{
		hid_device* handle = device->handle;
		JoyconDevice* devicePointer = device;
		cout << "Begin function";
		//unsigned char buf[0x40];
		memset(buf, 0, 0x40);
		cout << "passed memset";
		hid_set_nonblocking(handle,0);
		cout << "set nonblocking";
		buf[0] = 0x01;
		//enable vibration
		
		send_subcommand(device->handle, 0x1, 0x48, buf, 1);
		cout << "vibration enabled";
		//enable IMU
		send_subcommand(device->handle, 0x1, 0x40, buf, 1);
		cout << "IMU enabled";
		buf[0] = 0x30;
		send_subcommand(device->handle, 0x1, 0x03, buf, 1);
		cout << "Input report set";
		//do I get, yeah get callibration here
		GetCalibration(device);
		cout << "calibration got";

	}
	EXPORT void Close(JoyconDevice* device)
	{
		device->running = false;
		hid_close(device->handle);
	}
	EXPORT void Input_Update_Report(JoyconDevice* device)
	{
		if (device->handle == nullptr)
			return;
		while (device->running)
		{
			uint8_t packet[0x170];
			memset(packet, 0, 0x170);
			hid_read_timeout(device->handle, packet, 0x170, 20);
			
			//state intermediary
			JoyconState inputState;

			
			if (packet[0] == 0x21 || packet[0] == 0x30 || packet[0] == 0x31 ||
				packet[0] == 0x32 || packet[0] == 0x33)
			{
				// button datapull
				int offset = 3;
				uint8_t* button_data = packet + offset;

				if (device->leftRight)
				{
					device->state.buttons = (button_data[1] << 8) || (button_data[0]);
				}
				else
				{
					device->state.buttons = (button_data[1] << 8) || (button_data[2]);
				}
				// stick data pull
				offset = (device->leftRight) ? 6 : 9;
				uint8_t* stick_data = packet + offset;

				device->state.stick.raw_x = (stick_data[0] | ((stick_data[1] & 0xF) << 8));
				device->state.stick.raw_y = ((stick_data[1] >> 4) | (stick_data[2] << 4));

				CalcAnalogStick(device->state.stick.x, device->state.stick.y,
					device->state.stick.raw_x, device->state.stick.raw_y,
					device->calibration.stickCalibration.x_cal, device->calibration.stickCalibration.y_cal);				
			}			
			//run again
		}
	}

	EXPORT uint16_t GetButtons(JoyconDevice* device)
	{
		return device->state.buttons;
	}
	EXPORT float GetXStickAxis(JoyconDevice* device)
	{
		return device->state.stick.x;
	}
	EXPORT float GetYStickAxis(JoyconDevice* device)
	{
		return device->state.stick.y;
	}


	int Main()
	{
		JoyconDevice* device = getHandle(true);

		init_bt(device);


		char x;
		cin >> x;
		return 0;
	}

