﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Microsoft.Xna.Framework.Input;
namespace JoyGame
{
    class JCPStateContainer
    {
        public JoyconPairPlayer player;
        public JoyconPairState oldState;
        public JoyconPairState State;

        public void Update()
        {
            oldState = State;
            State = JoyconPair.GetState(player);
        }

        public bool ButtonDown(JPairButton Button)
        {
            return State.IsButtonDown(Button);
        }
        public bool ButtonUp(JPairButton Button)
        {
            return State.IsButtonUp(Button);
        }
        public bool ButtonPress(JPairButton Button)
        {
            return (oldState.IsButtonUp(Button) && State.IsButtonDown(Button));
        }
        public bool ButtonRelease(JPairButton Button)
        {
            return (oldState.IsButtonDown(Button) && State.IsButtonUp(Button));
        }
    }

    class JoyconPairStateCollection
    {
        private JCPStateContainer[] _pairs;
        private Dictionary<JoyconPairPlayer, int> _indexLookup;

        public JoyconPairStateCollection()
        {
            _pairs = new JCPStateContainer[2]
            {
                new JCPStateContainer(){
                player = JoyconPairPlayer.PairOne
                ,State = JoyconPairState.Default},
                new JCPStateContainer(){
                    player = JoyconPairPlayer.PairTwo
                ,State = JoyconPairState.Default}
            };
            _indexLookup = new Dictionary<JoyconPairPlayer, int>(2)
            {
                {JoyconPairPlayer.PairOne,0 },
                {JoyconPairPlayer.PairTwo,1 }
            };
        }

        public JCPStateContainer this[JoyconPairPlayer index]
        {
            get
            {
                return _pairs[_indexLookup[index]];
            }
        }

    }
}
