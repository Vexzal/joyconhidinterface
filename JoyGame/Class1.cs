﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Input;

namespace JoyGame
{
    partial class Game1 // pair run
    {
        protected void PairGenerationListenerUpdate(GameTime gameTime)
        {
            if(pairCollection[JoyconPairPlayer.PairOne].ButtonPress(JPairButton.Minus))
            {
                ChangeState(GameState.DebugState);
            }

            for(int i = 0;i<5;i++)
            {
                var index = Joycon.GetState(i);
                if (index.IsButtonDown(JoyconButtons.LR))
                {
                    if (index.LeftRight && !rightAdded)
                    {
                        right = (JoyconIndex)i;
                        rightAdded = true;
                    }
                    if(!index.LeftRight && !leftAdded)
                    {
                        left = (JoyconIndex)i;
                        leftAdded = true;
                    }
                }
            }

            if(leftAdded && rightAdded)
            {
                JoyconPair.AddDevice(left, right);

                ChangeState(GameState.DebugState);
                leftAdded = false;
                rightAdded = false;
            }

        }

        

        bool leftAdded = false;
        JoyconIndex left;
        bool rightAdded = false;
        JoyconIndex right;

        EditorCamera camera;

        protected void PairGenerationListenerDraw(GameTime gameTime)
        {
            Vector2 left = new Vector2(graphics.PreferredBackBufferWidth / 4,
                graphics.PreferredBackBufferHeight / 2);
            Vector2 right = new Vector2((graphics.PreferredBackBufferWidth / 4)*3, 
                graphics.PreferredBackBufferHeight / 2);

            Vector2 center = new Vector2(graphics.PreferredBackBufferWidth / 2, 
                graphics.PreferredBackBufferHeight/2);
            
            spriteBatch.Begin();
            spriteBatch.DrawString(font, "Awaiting Input", center - (font.MeasureString("Awaiting Input") / 2), Color.Purple);
            if (leftAdded)
                spriteBatch.DrawString(font, "LEFT ADDED", left - (font.MeasureString("LEFT ADDED") / 2), Color.Purple);
            if (rightAdded)
                spriteBatch.DrawString(font, "RIGHT ADDED", right - (font.MeasureString("RIGHT ADDED") / 2), Color.Purple);

            spriteBatch.End();

        }
    }

    partial class Game1//debug run
    {
        JoyconPairStateCollection pairCollection = new JoyconPairStateCollection();

        Model Cube;
        Matrix CubeTranslation = Matrix.Identity;
        Matrix View = Matrix.CreateLookAt(Vector3.UnitY * 10, Vector3.Zero, Vector3.UnitZ);
        Matrix Projection;


        private class CubeDraw
        {
            public Color Color;
            public Matrix Translation;
            public CubeDraw(Color color, Matrix translation)
            {
                Color = color;
                Translation = translation;
            }
            public void Draw(Model cube,Matrix View, Matrix Projection)
            {
                foreach(ModelMesh mesh in cube.Meshes)
                {
                    foreach(BasicEffect b in mesh.Effects)
                    {
                        b.DiffuseColor = Color.ToVector3();
                        b.EnableDefaultLighting();
                        b.World = Translation;
                        b.View = View;
                        b.Projection = Projection;
                    }
                    mesh.Draw();
                }
                
            }
        }

        List<CubeDraw> cubePixels = new List<CubeDraw>();
        protected void DebugUpdate(GameTime gameTime)
        {
            BackgroundColor = Color.White;

            if(pairCollection[JoyconPairPlayer.PairOne].ButtonDown(JPairButton.L))
            {
                BackgroundColor = Color.Red;
                CubeTranslation = Matrix.Identity;
            }
            if(pairCollection[JoyconPairPlayer.PairOne].ButtonDown(JPairButton.ZL))
            {
                var tempCube = new CubeDraw(Color.Green, CubeTranslation);
                if (!cubePixels.Contains(tempCube))
                    cubePixels.Add(tempCube);
            }
            if(pairCollection[JoyconPairPlayer.PairOne].ButtonDown(JPairButton.Minus))
            {
                cubePixels.Clear();
            }
            if(pairCollection[JoyconPairPlayer.PairOne].ButtonDown(JPairButton.R))
            {
                BackgroundColor = Color.Blue;
            }
            if (pairCollection[JoyconPairPlayer.PairTwo].ButtonDown(JPairButton.L))
            {
                BackgroundColor = Color.Pink;
            }
            if (pairCollection[JoyconPairPlayer.PairTwo].ButtonDown(JPairButton.R))
            {
                BackgroundColor = Color.LightGreen;
            }

            if(pairCollection[JoyconPairPlayer.PairOne].ButtonPress(JPairButton.Capture))
            {
                ChangeState(GameState.PairingState);
            }
            CubeTranslation *= Matrix.CreateTranslation(
                new Vector3(
                    pairCollection[JoyconPairPlayer.PairOne].State.LeftGyroVector().Z, 
                    0,
                    -pairCollection[JoyconPairPlayer.PairOne].State.LeftGyroVector().X));


        }
        protected void DebugDraw(GameTime gameTime)
        {
            foreach(var pixel in cubePixels)
            {
                pixel.Draw(Cube, View, Projection);
            }

            foreach(var mesh in Cube.Meshes)
            {
                foreach(BasicEffect effect in mesh.Effects)
                {
                    effect.DiffuseColor = Color.Green.ToVector3();
                    effect.EnableDefaultLighting();
                    effect.World = CubeTranslation;
                    effect.View = Matrix.CreateLookAt(Vector3.UnitY * 10, Vector3.Zero, Vector3.UnitZ);
                    effect.Projection = Matrix.CreatePerspectiveFieldOfView(MathHelper.ToRadians(30), GraphicsDevice.Adapter.CurrentDisplayMode.AspectRatio, 1, 100);
                }
                mesh.Draw();
            }
            //Cube.Draw(CubeTranslation, Matrix.CreateLookAt(Vector3.UnitY * 10, Vector3.Zero, Vector3.UnitZ),Matrix.CreatePerspectiveFieldOfView(MathHelper.ToRadians(30),GraphicsDevice.Adapter.CurrentDisplayMode.AspectRatio,1,100));
            //spriteBatch.Begin();
            //DebugDrawState(pairCollection[JoyconPairPlayer.PairOne].State, Vector2.Zero);
            ////DebugDrawState(pairCollection[JoyconPairPlayer.PairTwo].State,
            ////    new Vector2(0,60));
            //spriteBatch.End();
        }

        protected void DebugDrawState(JoyconPairState state, Vector2 offset)
        {
            spriteBatch.DrawString(font,
                "Connected: " + state.Connected,
                offset, Color.Purple);
            spriteBatch.DrawString(font,
                "RgithStick: " + state.RightStick() + "  |  " + "LeftStick: " + state.LeftStick(), offset + new Vector2(0, 20), Color.Purple);

            spriteBatch.DrawString(font,
                "Left Accelerometer: " + "\n    " + state.LeftAccel().X + "\n    " + state.LeftAccel().Y + "\n    " + state.LeftAccel().Z + "\n" +
                "Right Accelerometer: " + "\n    " + state.RightAccel().X + "\n    " + state.RightAccel().Y + "\n    " + state.RightAccel().Z,
                    offset += new Vector2(0, 40), Color.Purple);
            spriteBatch.DrawString(font,
                "Left Gyrescope: " + "\n    " + state.LeftGyroVector().X + "\n    " + state.LeftGyroVector().Y + "\n    " + state.LeftGyroVector().Z + "\n" + "\n" +
                "Right Gyrescope: " + "\n    " + state.RightGyroVector().X + "\n    " + state.RightGyroVector().Y + "\n    " + state.RightGyroVector().Z + "\n",
                offset + new Vector2(0, 200), Color.Purple);


        }

        

    }

    public class EditorCamera
    {
        public float xAngel;
        public float zAngel;

        Matrix focus;
        Matrix pivot;
        Matrix arm;
        Matrix camera;

        public Matrix View
        {
            get => Matrix.CreateLookAt(
                Vector3.Transform(Vector3.Zero,
                    Matrix.CreateTranslation(0, -distance, 0) * pivot),
                focus.Translation,
                Up);
        }

        public Matrix Projection
        {
            get => Matrix.CreatePerspectiveFieldOfView(FOV, aspect, nearClipPlane, farClipPlane);
        }
        //camera settings
        public float distance;
        public Vector3 Up;
        //projection settings
        public float FOV;
        public float nearClipPlane;
        public float farClipPlane;
        public float aspect;
        public EditorCamera(GraphicsDevice device, float distance)
        {
            xAngel = -45;
            zAngel = 45;

            aspect = device.Viewport.AspectRatio;
            FOV = MathHelper.ToRadians(30);
            nearClipPlane = 1;
            farClipPlane = 100;
            this.distance = distance;
            Up = Vector3.UnitZ;

            pivot = Matrix.CreateRotationX(MathHelper.ToRadians(xAngel)) * Matrix.CreateRotationZ(MathHelper.ToRadians(zAngel));//Matrix.CreateFromYawPitchRoll(0, MathHelper.ToRadians(xAngel), MathHelper.ToRadians(zAngel));
            arm = Matrix.CreateTranslation(new Vector3(0, -distance, 0));
            camera = Matrix.CreateLookAt(Vector3.UnitY, Vector3.Zero, Up);
        }
        public void UpdatePivot()
        {
            //pivot = Matrix.CreateFromYawPitchRoll(
            //    0,
            //    MathHelper.ToRadians(xAngel),
            //    MathHelper.ToRadians(zAngel));
            pivot = Matrix.CreateRotationX(MathHelper.ToRadians(xAngel))
                * Matrix.CreateRotationY(0)
                * Matrix.CreateRotationZ(MathHelper.ToRadians(zAngel));
        }
        public void UpdateProjection()
        {

        }
        public void UpdateCamera()
        {
            camera = Matrix.CreateLookAt(Vector3.UnitY, Vector3.Zero, Up);

        }
        public Vector3 GetCameraZ()
        {
            Matrix getView = View;
            return Vector3.Normalize(new Vector3(getView.M13, getView.M23, getView.M33));
        }
        public Vector3 GetCameraY()
        {
            Matrix getView = View;
            return Vector3.Normalize(new Vector3(getView.M12, getView.M22, getView.M32));
        }
        public Vector3 GetCameraX()
        {
            Matrix getView = View;
            return Vector3.Normalize(new Vector3(getView.M11, getView.M21, getView.M31));
        }
        public Vector3 GetCameraPosition()
        {
            return Vector3.Transform(Vector3.Zero,
                     Matrix.CreateTranslation(0, -distance, 0) * pivot);
        }
        public BoundingFrustum CameraFrustrum
        {
            get { return new BoundingFrustum(View * Projection); }
        }
    }
}
