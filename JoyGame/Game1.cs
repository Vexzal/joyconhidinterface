﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;


namespace JoyGame
{
    public enum GameState
    {
        PairingState,
        DebugState,
        MenuState
    }


    /// <summary>
    /// This is the main type for your game.
    /// </summary>
    public partial class Game1 : Game
    {
        GraphicsDeviceManager graphics;
        SpriteBatch spriteBatch;
        SpriteFont font;

        GameState RunState = GameState.PairingState;

        //JoyconState oldLeft;
        //JoyconState newLeft;
        //JoyconState oldRight;
        //JoyconState newRight;

        public Game1()
        {
            graphics = new GraphicsDeviceManager(this);
            Content.RootDirectory = "Content";
        }

        /// <summary>
        /// Allows the game to perform any initialization it needs to before starting to run.
        /// This is where it can query for any required services and load any non-graphic
        /// related content.  Calling base.Initialize will enumerate through any components
        /// and initialize them as well.
        /// </summary>
        protected override void Initialize()
        {
            // TODO: Add your initialization logic here
            
            this.Exiting += CloseJoycon;
            graphics.PreferredBackBufferHeight = graphics.PreferredBackBufferHeight * 2;
            graphics.PreferredBackBufferWidth = graphics.PreferredBackBufferWidth * 2;
            graphics.ApplyChanges();
            Joycon.Initialize();
            base.Initialize();
        }

        private void CloseJoycon(object sender, System.EventArgs e)
        {
            Joycon.CloseDevices();
        }

        /// <summary>
        /// LoadContent will be called once per game and is the place to load
        /// all of your content.
        /// </summary>
        protected override void LoadContent()
        {
            // Create a new SpriteBatch, which can be used to draw textures.
            Cube = Content.Load<Model>("JoyCube");
            Projection = Matrix.CreatePerspectiveFieldOfView(MathHelper.ToRadians(30), GraphicsDevice.Adapter.CurrentDisplayMode.AspectRatio, 1, 100); ;
            spriteBatch = new SpriteBatch(GraphicsDevice);
            font = Content.Load<SpriteFont>("debugFont");
            // TODO: use this.Content to load your game content here
        }

        /// <summary>
        /// UnloadContent will be called once per game and is the place to unload
        /// game-specific content.
        /// </summary>
        protected override void UnloadContent()
        {
            // TODO: Unload any non ContentManager content here
        }

        /// <summary>
        /// Allows the game to run logic such as updating the world,
        /// checking for collisions, gathering input, and playing audio.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Update(GameTime gameTime)
        {
            if (GamePad.GetState(PlayerIndex.One).Buttons.Back == ButtonState.Pressed || Keyboard.GetState().IsKeyDown(Keys.Escape))
                Exit();
            pairCollection[JoyconPairPlayer.PairOne].Update();
            pairCollection[JoyconPairPlayer.PairTwo].Update();
            // TODO: Add your update logic here
            //oldLeft = newLeft;
            //oldRight = newRight;

            //newLeft = Joycon.GetState(JoyconIndex.Left);
            //newRight = Joycon.GetState(JoyconIndex.Right);
            //if (newLeft.IsButtonDown(JoyconButtons.LR))
            //    BackgroundColor = Color.Red;
            //if (newRight.IsButtonDown(JoyconButtons.LR))
            //    BackgroundColor = Color.Blue;
            //if (newLeft.IsButtonUp(JoyconButtons.LR) && newRight.IsButtonUp(JoyconButtons.LR))
            //    BackgroundColor = Color.White;
            //if (Keyboard.GetState().IsKeyDown(Keys.Space))
            //    BackgroundColor = Color.Green;

            switch (RunState)
            {
                case GameState.PairingState:
                    PairGenerationListenerUpdate(gameTime);
                    break;
                case GameState.MenuState:
                    break;
                case GameState.DebugState:
                    DebugUpdate(gameTime);
                    break;
            }


            base.Update(gameTime);
        }
        Color BackgroundColor = Color.White;
        /// <summary>
        /// This is called when the game should draw itself.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Draw(GameTime gameTime)
        {
            GraphicsDevice.Clear(BackgroundColor);


            switch(RunState)
            {
                case GameState.PairingState:
                    PairGenerationListenerDraw(gameTime);
                    break;
                case GameState.DebugState:
                    DebugDraw(gameTime);
                    break;
            }
            //spriteBatch.Begin();
            //spriteBatch.DrawString(font, "" + newLeft.StickValue(), Vector2.Zero, Color.Purple);
            //spriteBatch.DrawString(font, "" + newLeft.GetGyroVec3(), new Vector2(0, 20), Color.Purple);
            //spriteBatch.DrawString(font, "" + newLeft.PacketNumber, new Vector2(0, 40), Color.Purple);
            //spriteBatch.End();
            // TODO: Add your drawing code here

            base.Draw(gameTime);
        }

        public void ChangeState(GameState state)
        {
            RunState = state;
        }
    }
}
